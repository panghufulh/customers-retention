package com.fulh.customers.service;

import com.fulh.customers.domain.User;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName UserService
 * @Description TODO
 * @Author fulh
 * @Date 2018/11/22 19:14
 * @Version 1.0
 */
public interface UserService {

    int addUser(User user);

    List<User> findAllUser(int pageNum, int pageSize);

    HashMap selectUser(int userId);
}
