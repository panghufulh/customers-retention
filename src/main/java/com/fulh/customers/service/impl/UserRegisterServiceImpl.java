package com.fulh.customers.service.impl;

import com.fulh.customers.domain.UserRegister;
import com.fulh.customers.mapper.UserRegisterMapper;
import com.fulh.customers.service.UserRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterServiceImpl implements UserRegisterService {

    @Autowired
    private UserRegisterMapper userRegisterMapper;

    @Override
    public int add(UserRegister userRegister) {
        return userRegisterMapper.insert(userRegister);
    }
}
