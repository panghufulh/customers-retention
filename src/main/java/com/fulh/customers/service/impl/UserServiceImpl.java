package com.fulh.customers.service.impl;

import com.fulh.customers.domain.User;
import com.fulh.customers.mapper.UserMapper;
import com.fulh.customers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author fulh
 * @Date 2018/11/22 19:18
 * @Version 1.0
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public int addUser(User user) {
        return userMapper.insertSelective(user);
    }

    @Override
    public List<User> findAllUser(int pageNum, int pageSize){ //将参数传给这个方法就可以实现物理分页了，非常简单。
        // PageHelper.startPage(pageNum, pageSize);
        return userMapper.selectAllUser();
    }

    @Override
    public HashMap selectUser(int userId) {
        return userMapper.selectUser(userId);
    }


}
