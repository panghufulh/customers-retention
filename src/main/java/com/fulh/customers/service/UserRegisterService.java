package com.fulh.customers.service;

import com.fulh.customers.domain.UserRegister;

public interface UserRegisterService {

    int add(UserRegister userRegister);
}
