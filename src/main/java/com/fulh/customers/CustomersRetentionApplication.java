package com.fulh.customers;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.fulh.customers.mapper")		//将项目中对应的mapper类的路径加进来
/**
 * @ClassName CustomersRetentionApplication
 * @Description 启动类
 * @author fulh
 * @date 2018/11/28 19:54
 * @Version 1.0
 */
public class CustomersRetentionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomersRetentionApplication.class, args);
	}
}
