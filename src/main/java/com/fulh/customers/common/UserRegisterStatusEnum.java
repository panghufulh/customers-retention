package com.fulh.customers.common;

public enum UserRegisterStatusEnum {
    ZERO("0","新建"),ONE("1","审批通过"),TWO("2","审批未通过");

    private String value;
    private String desc;

    UserRegisterStatusEnum(String value,String desc){
        this.value = value;
        this.desc = desc;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }
}
