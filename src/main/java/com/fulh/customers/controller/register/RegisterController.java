package com.fulh.customers.controller.register;

import com.fulh.customers.common.UserRegisterStatusEnum;
import com.fulh.customers.domain.UserRegister;
import com.fulh.customers.service.impl.UserRegisterServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @ClassName RegisterController
 * @Description TODO
 * @author fulh
 * @date 2018/11/28 19:54
 * @Version 1.0
 */
@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserRegisterServiceImpl userRegisterService;

    @RequestMapping("/add")
    @ResponseBody
    public String register(@RequestParam() Map<String,String> param){
        if(null == param){
            return "FAIL";
        }
        if(StringUtils.isBlank(param.get("phone"))  || StringUtils.isBlank(param.get("password"))
                || StringUtils.isBlank(param.get("repeat_password")) || StringUtils.isBlank(param.get("email"))
                || StringUtils.isBlank(param.get("account"))){
            return "FAIL";
        }
        String phone = String.valueOf(param.get("phone"));
        String password = String.valueOf(param.get("password"));
        String repeat_password = String.valueOf(param.get("repeat_password"));
        String email = String.valueOf(param.get("email"));
        String account = String.valueOf(param.get("account"));
        if(!password.equals(repeat_password)){
            return "FAIL";
        }
        UserRegister userRegister = new UserRegister();
        userRegister.setAccount(account);
        userRegister.setEmail(email);
        userRegister.setPassWord(password);
        userRegister.setPhone(phone);
        userRegister.setStatus(UserRegisterStatusEnum.ZERO.getValue());

        userRegisterService.add(userRegister);

        return "SUCCESS";
    }

}
