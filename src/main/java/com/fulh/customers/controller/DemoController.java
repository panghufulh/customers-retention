package com.fulh.customers.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @ClassName DemoController
 * @Description TODO
 * @Author fulh
 * @Date 2018/11/28 16:44
 * @Version 1.0
 */
@Controller
public class DemoController {

    @RequestMapping("/demo")
    public String demo(){
        System.out.println("1111...");
        return "demo";
    }

    @RequestMapping("/demo05")
    public String demo05(){

        return "demo05";
    }

    @RequestMapping("hello")
    @ResponseBody
    public String hello(){
        return "hello world....";
    }

    @RequestMapping("hi")
    //@ResponseBody
    public String hi(@RequestParam Map<String,Object> param){
        System.out.println("111111......");
        System.out.println(param);
        return "register/sucess";
    }
}
