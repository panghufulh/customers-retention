package com.fulh.customers.controller;

import com.fulh.customers.domain.User;
import com.fulh.customers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * @ClassName UserController
 * @Description TODO
 * @Author fulh
 * @Date 2018/11/22 19:14
 * @Version 1.0
 */

@Controller
@RequestMapping(value="/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value="/add",produces = {"application/json;charset=UTF-8"})
    public int addUser(User user){
        return userService.addUser(user);
    }

    @ResponseBody
    @RequestMapping(value = "/all/{pageNum}/{pageSize}", produces = {"application/json;charset=UTF-8"})
    public Object findAllUser(@PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize){
        return userService.findAllUser(pageNum,pageSize);
    }

    @ResponseBody
    @RequestMapping(value = "/getUserByUserId/{userId}", produces = {"application/json;charset=UTF-8"})
    public HashMap selectUser(@PathVariable("userId")String userId){
        return userService.selectUser(Integer.valueOf(userId));
    }

}
