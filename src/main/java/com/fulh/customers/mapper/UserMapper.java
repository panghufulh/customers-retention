package com.fulh.customers.mapper;

import com.fulh.customers.domain.User;

import java.util.HashMap;
import java.util.List;

public interface UserMapper {

    int insertSelective(User record);

    List<User> selectAllUser();

    HashMap selectUser(int userId);
}