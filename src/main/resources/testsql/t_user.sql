CREATE TABLE `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8

drop table if exists user;
create table if not exists user(
     user_id    int   UNSIGNED   PRIMARY KEY    AUTO_INCREMENT comment '自增长主键',
		 user_name  varchar(10)  not null comment '用户姓名',
		 user_mobile  varchar(11)  not null comment '用户手机号码',
		 user_auth    varchar(2)   not null comment '用户权限，0 超级管理员 1 部门管理员 2 普通管理员 3 普通员工',
     status       varchar(2)   not null comment '用户状态，U 正常  E 失效',
     permit_user_id int UNSIGNED  not null  comment '审批申请的人员编号',
     create_date  datetime     not null comment '用户创建时间',
     effective_date  datetime  not null comment '生效时间',
     expire_date     datetime  not null comment '失效时间',
     remarks         varchar(255)       comment '备注'
)engine=InnoDB AUTO_INCREMENT=1000 default charset=utf8 comment='系统使用人员表'