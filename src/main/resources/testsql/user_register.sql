create table if not exists user_register(
	id      int    UNSIGNED    auto_increment  PRIMARY key   comment '自增长主键',
	account    varchar(20)     not null      comment '登录账号',
	pass_word  varchar(20)     not null      comment '密码',
	phone      varchar(11)     not null      comment '手机号码',
  email      varchar(30)     not null      comment '注册邮箱',
	create_date  datetime      not null      comment '注册时间',
  status     varchar(2)      not null      comment '申请状态：0 新建申请  1 审批通过  2 审批未通过',
  chg_time   datetime                      comment '记录变更时间，即申请审批时间',
	verify_account  varchar(20)              comment '审批人账号',
	remarks    varchar(200)                  comment '备注'
)engine=InnoDB default charset=utf8 comment='申请注册表';